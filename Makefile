all: datetime

PKGS = glib-2.0

datetime: datetime.c
	$(CC) -g -o $@ datetime.c $(WARNINGS) $(shell pkg-config --cflags --libs $(PKGS))

clean:
	rm -f datetime
