/* datetime.c
 *
 * Copyright (C) 2010 Christian Hergert <chris@dronelabs.com>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <glib.h>

static void
print_todays_date (void)
{
	GDateTime *dt;
	gchar *str;

	dt = g_date_time_new_today();
	str = g_date_time_printf(dt, "Today is %x.");
	g_print("%s\n", str);

	g_date_time_unref(dt);
	g_free(str);
}

static void
print_tomorrows_date (void)
{
	GDateTime *dt;
	GDateTime *dt2;
	gchar *str;

	dt = g_date_time_new_today();
	dt2 = g_date_time_add_days(dt, 1);
	str = g_date_time_printf(dt2, "Tomorrow is %x.");
	g_print("%s\n", str);

	g_date_time_unref(dt);
	g_date_time_unref(dt2);
	g_free(str);
}

static void
print_yesterdays_date (void)
{
	GDateTime *dt;
	GDateTime *dt2;
	gchar *str;

	dt = g_date_time_new_today();
	dt2 = g_date_time_add_days(dt, -1);
	str = g_date_time_printf(dt, "Yesterday was %x.");
	g_print("%s\n", str);

	g_date_time_unref(dt);
	g_date_time_unref(dt2);
	g_free(str);
}

static void
print_current_timezone (void)
{
	GDateTime *dt;

	dt = g_date_time_new_now();
	g_print("The current timezone is %s.\n",
	        g_date_time_get_timezone_name(dt));
	g_date_time_unref(dt);
}

static void
print_is_leap_year (void)
{
	GDateTime *dt;

	dt = g_date_time_new_today();
	if (g_date_time_is_leap_year(dt)) {
		g_print("This year (%d) is a leap year.\n",
		        g_date_time_get_year(dt));
	} else {
		g_print("This year (%d) is not a leap year.\n",
		        g_date_time_get_year(dt));
	}
	g_date_time_unref(dt);
}

static void
print_days_until_birthday (void)
{
	GDateTime *now;
	GDateTime *bday;
	GTimeSpan diff;

	now = g_date_time_new_today();
	bday = g_date_time_new_from_date(g_date_time_get_year(now) + 1, 8, 16);
	diff = g_date_time_difference(now, bday);

	g_print("There are %ld days until Christian's birthday.\n",
			diff / G_TIME_SPAN_DAY);

	g_date_time_unref(now);
	g_date_time_unref(bday);
}

static gint
sorter (gconstpointer a,
        gconstpointer b)
{
	return g_date_time_compare(*(gpointer *)a, *(gpointer *)b);
}

static void
print_dates_sorted (void)
{
	GPtrArray *dates;
	GDateTime *dt;
	gchar *str;
	gint i;

	dates = g_ptr_array_new();
	g_ptr_array_set_free_func(dates, (GDestroyNotify)g_date_time_unref);

	/*
	 * Insert some random dates into the array.
	 */
	for (i = 0; i < 30; i++) {
		dt = g_date_time_new_from_date(g_random_int_range(1900, 2020),
		                               g_random_int_range(1, 12),
		                               g_random_int_range(1, 28));
		g_ptr_array_add(dates, dt);
	}

	/*
	 * Sort dates.  Remember that GPtrArray returns a pointer to the
	 * pointer type in sorting methods so they need to be dereferenced.
	 */
	g_ptr_array_sort(dates, sorter);

	/*
	 * Print out the dates
	 */
	g_print("Dates sorted in order:\n");
	for (i = 0; i < dates->len; i++) {
		dt = g_ptr_array_index(dates, i);
		str = g_date_time_printf(dt, "%b %d, %Y");
		g_print("  %s\n", str);
		g_free(str);
	}

	g_ptr_array_unref(dates);
}

gint
main (gint   argc,
      gchar *argv[])
{
	print_todays_date();
	print_tomorrows_date();
	print_yesterdays_date();
	print_current_timezone();
	print_is_leap_year();
	print_days_until_birthday();
	print_dates_sorted();
	return 0;
}
